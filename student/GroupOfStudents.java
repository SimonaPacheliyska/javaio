package student;

import java.util.Set;
import java.util.TreeSet;

public class GroupOfStudents {
    private Set<Student> students;

    public void addStudent(Student student) throws CloneNotSupportedException {
        students.add(student.clone());
    }

    public Set<Student> getAllStudents() {
        return new TreeSet<>(students);
    }

    public void remove(Student student) {
        students.remove(student);
    }
}
