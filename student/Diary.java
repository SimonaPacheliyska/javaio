package student;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class Diary implements StudentDiary {
    private GroupOfStudents students;

    @Override
    public double getAverageMark() {
        return students.getAllStudents().stream()
                .mapToDouble(student -> student.getMark()).average()
                .getAsDouble();
    }

    @Override
    public List<Student> getAllPassing() {
        return students.getAllStudents().stream()
                .filter(student -> student.getMark() >= 3)
                .collect(Collectors.toList());
    }

    @Override
    public List<Student> getAllFailing() {
        return students.getAllStudents().stream().filter(s -> s.getMark() < 3)
                .collect(Collectors.toList());
    }

    @Override
    public Map<Boolean, List<Student>> splitByMark(float mark) {
        return students.getAllStudents().stream().collect(
                Collectors.partitioningBy(student -> student.getMark() > mark));
    }

    @Override
    public List<Student> orderAscending() {
        return students.getAllStudents().stream().sorted()
                .collect(Collectors.toList());
    }

    @Override
    public List<Student> orderDescending() {
        return students.getAllStudents().stream()
                .sorted(Comparator.reverseOrder()).collect(Collectors.toList());
    }

    @Override
    public List<Student> getAllWithHighestMark() {
        float highest = students.getAllStudents().stream()
                .map(student -> student.getMark())
                .reduce((float) 2.0, Float::max);
        return students.getAllStudents().stream()
                .filter(student -> student.getMark() == highest)
                .collect(Collectors.toList());
    }

    @Override
    public List<Student> getAllWithLowestMark() {
        float lowest = students.getAllStudents().stream()
                .map(student -> student.getMark())
                .reduce((float) 2.0, Float::min);
        return students.getAllStudents().stream()
                .filter(student -> student.getMark() == lowest)
                .collect(Collectors.toList());
    }

    @Override
    public List<Float> getDistrByAge(byte age) {
        return students.getAllStudents().stream()
                .filter(student -> student.getAge() == age)
                .map(student -> student.getMark()).collect(Collectors.toList());
    }

    @Override
    public double getAvrgByGender(Gender gender) {
        return students.getAllStudents().stream()
                .filter(student -> student.getGender() == gender)
                .mapToDouble(student -> student.getMark()).average()
                .getAsDouble();
    }

    @Override
    public Map<Gender, Map<Byte, List<Student>>> split() {
        return students.getAllStudents().stream().collect(Collectors.groupingBy(
                Student::getGender, Collectors.groupingBy(Student::getAge)));
    }

    @Override
    public Map<Float, Long> getMarksByDistribution() {
        return students.getAllStudents().stream().collect(
                Collectors.groupingBy(Student::getMark, Collectors.counting()));
    }

    @Override
    public List<String> getAllMails() {
        return students.getAllStudents().stream()
                .map(student -> student.getEmail())
                .collect(Collectors.toList());
    }

}
