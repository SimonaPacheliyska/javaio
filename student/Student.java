package student;

import java.util.Comparator;

public class Student implements Comparable<Student>, Cloneable {
    private String firstName;
    private String lastName;
    private Gender gender;
    private byte age;
    private float mark;
    private String email;

    public Student(String firstName, String lastName, Gender gender, byte age,
            float mark, String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.age = age;
        this.mark = mark;
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Gender getGender() {
        return gender;
    }

    public byte getAge() {
        return age;
    }

    public float getMark() {
        return mark;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public int compareTo(Student o) {
        return Comparator.comparing(Student::getFirstName)
                .thenComparing(Student::getLastName).compare(this, o);
    }

    @Override
    public Student clone() throws CloneNotSupportedException {
        return (Student) super.clone();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((firstName == null) ? 0 : firstName.hashCode());
        result = prime * result + ((gender == null) ? 0 : gender.hashCode());
        result = prime * result
                + ((lastName == null) ? 0 : lastName.hashCode());
        result = prime * result + Float.floatToIntBits(mark);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Student other = (Student) obj;
        if (firstName == null) {
            if (other.firstName != null)
                return false;
        } else if (!firstName.equals(other.firstName))
            return false;
        if (gender != other.gender)
            return false;
        if (lastName == null) {
            if (other.lastName != null)
                return false;
        } else if (!lastName.equals(other.lastName))
            return false;
        if (Float.floatToIntBits(mark) != Float.floatToIntBits(other.mark))
            return false;
        return true;
    }
}
