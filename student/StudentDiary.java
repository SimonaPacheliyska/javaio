package student;

import java.util.List;
import java.util.Map;

public interface StudentDiary {

    public double getAverageMark();

    public List<Student> getAllPassing();

    public List<Student> getAllFailing();

    public Map<Boolean, List<Student>> splitByMark(float mark);

    public List<Student> orderAscending();

    public List<Student> orderDescending();

    public List<Student> getAllWithHighestMark();

    public List<Student> getAllWithLowestMark();

    public List<Float> getDistrByAge(byte age);

    public double getAvrgByGender(Gender gender);

    Map<Gender, Map<Byte, List<Student>>> split();

    Map<Float, Long> getMarksByDistribution();
    
    List<String> getAllMails();
}
